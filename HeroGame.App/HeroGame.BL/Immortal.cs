﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.BL
    {
    public delegate string HyperDelegate(string message);

    public class Immortal : AbstractHero, ISuperHero
        {

            private int _health;
            private int _exp;
            private int _strength;
            private string _name;


            public Immortal(string name)
            {
                _name = name;
                Health = 1000;
                Experience = 0;
                Strength = 0;
            }

            public override string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }
            public override int Health
            {
                get
                {
                    return _health;
                }
                set
                {
                _health = value;
                }

            }
            public override int Experience
            {
                get
                {
                    return _exp;
                }
                set
                {
                    _exp = value;
                }

            }
            public override int Strength
            {
                get
                {
                    return _strength;
                }
                set
                {
                    _strength = value;
                }

            }
        public override void Attack(AbstractHero enemy)
        {
            Strength--;
            enemy.Health--;
        }
        public override void Heal()
            {
                Health++;

            }
        
        public void HyperAttack(AbstractHero enemy)
        {
            Health -= 100;
            enemy.Health -= 10;
            hypermessage("hyperattack is done");
        }


        HyperDelegate hypermessage = HyperMethod;

        static string HyperMethod(string message)
        {
            throw new NotImplementedException();

        }
        public override void LevelUp()
            {
                Experience++;
            }
        

            public override string ToString()
            {
                return $"Name: {Name}\nHealth: {Health}\nExperience: {Experience}\nStrength: {Strength}";
            }
        }

    }

