﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.BL
{
    public class Monkey : AbstractHero
    {
        public event HyperDelegate MessageOutput;
        public Monkey(string name)
        {
            _name = Name;
            Health = 15;
            Experience = 5;
            Strength = 5;
        }
        private int _health;
        private int _exp;
        private int _strength;
        private string _name;


        public override int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;

            }
        }

        public override string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public override int Experience { get { return _exp; } set { _exp = value; } }
        public override int Strength { get => _strength; set { _strength = value; } }

        public void HyperMessageOutput(string message)
        {
            MessageOutput?.Invoke(message);
        }
        public override void Attack(AbstractHero enemy)
        {
            Strength--;
            enemy.Health--;
        }

        public override void Heal()
        {
            Health++;
        }

        public override void LevelUp()
        {
            Experience++;
        }
        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Health}\nExperience: {Experience}\nStrength: {Strength}";
        }
    }
}
