﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.BL
{
    public interface ISuperHero
    {
        void HyperAttack(AbstractHero enemy);

    }
}
