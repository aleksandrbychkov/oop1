﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.BL
{
    public abstract class AbstractHero
    {
        public abstract int Health
        {
            get;set;
            


        }
        public abstract int Experience
        {
            get;set;
            

        }
        public abstract int Strength
        {
            get;set;
        }
        public abstract string Name
        {
            get;set;
        }
        public abstract void Heal();
        public abstract void LevelUp();
        public abstract void Attack(AbstractHero enemy);
        public override bool Equals(object parameter)
        {
            AbstractHero abs = (AbstractHero)parameter;
            return this.Health == abs.Health;
        }

    }
}
