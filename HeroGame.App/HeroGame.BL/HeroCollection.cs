﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.BL
{
    public class HeroCollection: IList<AbstractHero>
    {
        List<AbstractHero> heroCollection = new List<AbstractHero>();

        public AbstractHero this[int index] { get => ((IList<AbstractHero>)heroCollection)[index]; set => ((IList<AbstractHero>)heroCollection)[index] = value; }

        public int Count => ((IList<AbstractHero>)heroCollection).Count;

        public bool IsReadOnly => ((IList<AbstractHero>)heroCollection).IsReadOnly;

        public void Add(AbstractHero item)
        {
            ((IList<AbstractHero>)heroCollection).Add(item);
        }

        public void Clear()
        {
            ((IList<AbstractHero>)heroCollection).Clear();
        }

        public bool Contains(AbstractHero item)
        {
            return ((IList<AbstractHero>)heroCollection).Contains(item);
        }

        public void CopyTo(AbstractHero[] array, int arrayIndex)
        {
            ((IList<AbstractHero>)heroCollection).CopyTo(array, arrayIndex);
        }

        public IEnumerator<AbstractHero> GetEnumerator()
        {
            return ((IList<AbstractHero>)heroCollection).GetEnumerator();
        }

        public int IndexOf(AbstractHero item)
        {
            return ((IList<AbstractHero>)heroCollection).IndexOf(item);
        }

        public void Insert(int index, AbstractHero item)
        {
            ((IList<AbstractHero>)heroCollection).Insert(index, item);
        }

        public bool Remove(AbstractHero item)
        {
            return ((IList<AbstractHero>)heroCollection).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<AbstractHero>)heroCollection).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IList<AbstractHero>)heroCollection).GetEnumerator();
        }
    }
}
