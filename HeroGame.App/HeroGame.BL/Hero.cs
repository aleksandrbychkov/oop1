﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.BL
{
    public class Hero : AbstractHero
    {

        private int _health;
        private int _exp;
        private int _strength;
        private string _name;


        public Hero(string name)
        {
            _name = name;
            Health = 10;
            Experience = 0;
            Strength = 10;
        }

        public override string Name{
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public override int Health
        {
            get
            {
                return _health;
            }
            set
            {
                _health = value;
            }

        }
        public override int Experience
        {
            get
            {
                return _exp;
            }
            set
            {
                _exp = value;
            }

        }
        public override int Strength
        {
            get
            {
                return _strength;
            }
            set
            {
                _strength = value;
            }

        }

        public override void Attack(AbstractHero enemy)
        {
            Strength--;
            enemy.Health--;
        }
        public override void Heal()
        {
            Health++;

        }
        public override void LevelUp()
        {
            Experience++;
        }

        

        public override string ToString()
        {
            return $"Name: {Name}\nHealth: {Health}\nExperience: {Experience}\nStrength: {Strength}";
        }
    }
    
}
