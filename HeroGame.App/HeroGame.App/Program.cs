﻿using HeroGame.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeroGame.App
{
    class Program
    {
        public event HyperDelegate MessageOutput;
        static void Main(string[] args)
        {
            Console.Write("Write name of your hero: ");
            string name = Console.ReadLine();
            AbstractHero Stasik = new Hero(name);
            AbstractHero Vovchik = new Monkey(name);
            AbstractHero Petya = new Immortal(name);
            Console.WriteLine("Type any of specified commands for work:");
            bool work = true;
            while (work) { 
            string option = Console.ReadLine();
            switch (option)
            {
                case "attack":
                    Stasik.Attack(Vovchik);
                        Console.WriteLine($"{Stasik.Name} attacked. Strength reduced");
                    break;
                case "heal":
                    Stasik.Heal();
                        Console.WriteLine($"{Stasik.Name} healed ");
                    break;
                case "levelup":
                    Stasik.LevelUp();
                    Console.WriteLine($"{Stasik.Name} level upped");
                    break;
                case "stats":
                    Console.WriteLine(Stasik);
                    break;
               case "exit":
                        work = false;
                        break;
                    case "compare":
                        Console.WriteLine(Stasik.Equals(Vovchik));
                        break;
                default:
                        Console.WriteLine("Error");
                        
                        break;
            }

            }
           
            Console.ReadKey();
        }
        
        
    }
}
